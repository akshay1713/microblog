package main

import (
	"crypto/rand"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

type UserAction struct {
	Username string
	Password string
	Action   string
	Content  string
	ID       int
}

type PubSub struct {
	Action            string
	SubscriberID      int
	PublisherID       int
	PublisherUsername string
}

type LogEntry struct {
	UUID       string
	Operation  string
	UserAction UserAction
	Pubsub     PubSub
	Timestamp  int64
}

func (logEntry *LogEntry) getJSONString() string {
	if logEntry.Operation == "user" {
		//Registration log entry
		data, err := json.Marshal(&logEntry.UserAction)
		if err != nil {
			fmt.Println("While marshalling json", err)
		}
		return string(data)
	}
	if logEntry.Operation == "pubsub" {
		//pubsub log entry
		data, err := json.Marshal(&logEntry.Pubsub)
		if err != nil {
			fmt.Println("While marshalling json", err)
		}
		return string(data)
	}
	return ""
}

type RaftLog struct {
	tableName         string
	database          *sql.DB
	uncommitedEntries map[string]LogEntry
}

func (raftLog *RaftLog) initialize() {
	//Create a table name by replacing periods and colons with underscores in the IP address of the node
	raftLog.tableName = strings.Replace(strings.Replace(localAddress+"_raft", ".", "_", -1), ":", "_", -1)
	database, err := sql.Open("sqlite3", logDbName)
	raftLog.database = database
	if err != nil {
		panic(err)
	}
	query := fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (id INTEGER PRIMARY KEY, operation TEXT, data TEXT, time timestamp)", raftLog.tableName)
	statement, err := raftLog.database.Prepare(query)
	if err != nil {
		panic(err)
	}
	_, err = statement.Exec()
	if err != nil {
		panic(err)
	}
	raftLog.uncommitedEntries = make(map[string]LogEntry)
}

func (raftLog *RaftLog) addAndInitiateCommit(newEntry LogEntry) {
	if localState.raftState == "leader" {
		//This node is the leader, initiate commit
		raftLog.add(newEntry)
		voteCount := localState.Cluster.voteOnLog(newEntry)
		if voteCount >= localState.Cluster.Count/2 {
			localState.Cluster.commitLog(newEntry)
			raftLog.commit(newEntry.UUID)
		}
	} else {
		//Commit to be initiated from the leader
		//The error should be handled here
		localState.Cluster.proposeCommit(newEntry)
	}
}

//Add a command to the raft log, but don't commit it
func (raftLog *RaftLog) add(newEntry LogEntry) {
	uuid := newEntry.UUID
	raftLog.uncommitedEntries[uuid] = newEntry
}

//Commit a previously added command to the raft log
func (raftLog *RaftLog) commit(uuid string) error {
	entry := raftLog.uncommitedEntries[uuid]
	raftLog.performCommitOperation(entry)
	timestamp := entry.Timestamp
	query := fmt.Sprintf("INSERT INTO `%s` values(null, '%s', %s, %d)", raftLog.tableName, entry.Operation, entry.getJSONString(), timestamp)
	statement, err := raftLog.database.Prepare(query)
	if err != nil {
		return err
	}
	_, err = statement.Exec()
	if err != nil {
		return err
	}
	return nil
}

func (raftLog *RaftLog) performCommitOperation(logEntry LogEntry) {
	store := getStore()
	switch logEntry.Operation {
	case "user":
		if logEntry.UserAction.Action == "register" {
			fmt.Println("Creating user")
			id, err := store.createUser(logEntry.UserAction.Username, logEntry.UserAction.Password, logEntry.UserAction.ID)
			if err != nil {
				fmt.Println("Error while creating user entry", err)
			} else {
				fmt.Println("User created with id ", id)
			}
		} else if logEntry.UserAction.Action == "post" {
			fmt.Println("Creating new post")
			id, err := store.createPost(logEntry.UserAction.Content, logEntry.UserAction.ID, logEntry.Timestamp)
			if err != nil {
				fmt.Println("Error while creating user entry", err)
			} else {
				fmt.Println("Post created with id ", id)
			}
		}
	case "pubsub":
		if logEntry.Pubsub.Action == "subscribe" {
			fmt.Println("Subscribing")
			err := store.subscribeUser(logEntry.Pubsub.SubscriberID, logEntry.Pubsub.PublisherUsername)
			if err != nil {
				fmt.Println("Error while subscribing to user", err)
			} else {
				fmt.Println("User subscription succeeded")
			}
		}
	}
}

func (raftLog *RaftLog) genBasicEntry() LogEntry {
	uuid, _ := raftLog.getUUID()
	logEntry := LogEntry{}
	logEntry.UUID = uuid
	logEntry.Timestamp = time.Now().Unix()
	return logEntry
}

func (raftLog *RaftLog) genEntryForUserCreation(user User) LogEntry {
	logEntry := raftLog.genBasicEntry()
	logEntry.Operation = "user"
	logEntry.UserAction = UserAction{
		Action:   "register",
		Username: user.Username,
		Password: user.Password,
		ID:       user.ID,
	}
	return logEntry
}

func (raftLog *RaftLog) genEntryForSubscription(subscriber User, publisher User) LogEntry {
	logEntry := raftLog.genBasicEntry()
	logEntry.Operation = "pubsub"
	logEntry.Pubsub = PubSub{
		Action:            "subscribe",
		SubscriberID:      subscriber.ID,
		PublisherID:       publisher.ID,
		PublisherUsername: publisher.Username,
	}
	return logEntry
}

func (raftLog *RaftLog) genEntryForPost(poster User, content string) LogEntry {
	logEntry := raftLog.genBasicEntry()
	logEntry.Operation = "user"
	logEntry.UserAction = UserAction{
		Username: poster.Username,
		Action:   "post",
		Content:  content,
		ID:       poster.ID,
	}
	return logEntry
}

func (raftLog *RaftLog) getUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		fmt.Println("Error while generating uuid", err)
		return "", err
	}
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}
