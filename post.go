package main

type Post struct {
	Content    string
	AuthorID   int
	Timestamp  int64
	ID         int
	AuthorName string
}

type PostManager struct {
}

func (postManager *PostManager) getPostsBy(username string, timestamp int64) ([]Post, error) {
	isRemote, shard := getShardForUsername(username)
	posts := []Post{}
	var err error
	if !isRemote {
		posts, err = postManager.getLocalPosts(username, timestamp)
	} else {
		posts, err = shard.Cluster.getPostsBy(username, timestamp)
	}
	for i := range posts {
		//Fix this idiocy later
		posts[i].AuthorName = username
	}
	return posts, err
}

func (postManager *PostManager) getLocalPosts(username string, timestamp int64) ([]Post, error) {
	store := getStore()
	user, err := store.getUser(username)
	if err != nil {
		return []Post{}, err
	}
	return store.getPosts(user.ID, timestamp)
}

var postManager PostManager

func getPostManager() PostManager { return postManager }
