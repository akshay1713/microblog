package main

import (
	"errors"
	"fmt"
)

type UserAccessor struct{}

func (userAccessor *UserAccessor) GetServer(username *string, server *string) error {
	*server = localState.getServerForUsername(*username)
	fmt.Println("Returning address ", *server)
	return nil
}

func (userAccessor *UserAccessor) Register(user *User, created *bool) error {
	fmt.Println("Searching for username")
	foundUser, err := localState.getUser(user.Username)
	if foundUser.Username == user.Username {
		fmt.Println("Found user with username ", foundUser.Username)
		//Username is not available
		return errors.New("A user with this username already exists")
	}
	//Username available
	localState.createUser(*user)
	return err
}

func (userAccessor *UserAccessor) Login(user *User, loggedIn *bool) error {
	err := localState.login(*user)
	if err == nil {
		*loggedIn = true
	}
	return err
}

func (userAccessor *UserAccessor) Subscribe(subscriptionArgs []*string, ack *bool) error {
	subscriberName := *subscriptionArgs[0]
	publisherName := *subscriptionArgs[1]
	*ack = true
	return localState.subscribe(subscriberName, publisherName)
}

func (userAccessor *UserAccessor) GetAllUsers(dummyArg *bool, users *[]User) error {
	usersFound, err := localState.getAllUsers()
	*users = usersFound
	return err
}

func (userAccessor *UserAccessor) Post(postArgs []*string, ack *bool) error {
	username := *postArgs[0]
	content := *postArgs[1]
	*ack = true
	return localState.post(username, content)
}

func (userAccessor *UserAccessor) GetPublishers(username *string, publishers *[]string) error {
	foundPublishers, err := localState.getPublishers(*username)
	*publishers = foundPublishers
	return err
}

func (userAccessor *UserAccessor) GetPosts(username *string, posts *[]Post) error {
	session := getSession()
	fmt.Println(session.latestPosts, session.previousPosts, "Are the current stores")
	foundPosts := session.getPostsForUser(*username)
	*posts = foundPosts
	return nil
}
