package main

type ShardAccessor struct {
}

func (shardAccessor *ShardAccessor) Announce(state *State, response *State) error {
	*response = localState
	err := localState.update(*state)
	return err
}
