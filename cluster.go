package main

import (
	"fmt"
	"net/rpc"
)

type Cluster struct {
	Peers map[string]*Peer
	Count int
}

func (cluster *Cluster) update(address string) {
	fmt.Println("Adding to cluster while updating - ", address)
	_, exists := cluster.Peers[address]
	if exists {
		fmt.Println("Peer with address ", address, " already exists, skipping")
		return
	}
	client, err := rpc.Dial("tcp", address)
	if err != nil {
		fmt.Println("Error while dialing peer", err)
		return
	}
	peer := Peer{
		Address: address,
		client:  client,
	}
	if cluster.Peers == nil {
		//First peer
		cluster.Peers = make(map[string]*Peer)
		cluster.Count = 1
	} else {
		cluster.Count++
	}
	cluster.Peers[address] = &peer
}

func (cluster *Cluster) updateRaftState() {
	for _, peer := range cluster.Peers {
		_, err := peer.getState(true)
		if err != nil {
			fmt.Println("Error while getting state", err)
		}
	}
}

func (cluster *Cluster) getLeader() Peer {
	for _, peer := range cluster.Peers {
		if peer.isLeader() {
			return *peer
		}
	}
	cluster.updateRaftState()
	//infinite loop possibility here, however if that happens it means there's a significant problem I'm not aware of
	return cluster.getLeader()
}

func (cluster *Cluster) sendHeartbeat() {
	for _, peer := range cluster.Peers {
		peer.sendHeartbeat()
	}
}

func (cluster *Cluster) getVotes() int {
	voteCount := 0
	for _, peer := range cluster.Peers {
		vote, err := peer.getVote()
		if err == nil && vote {
			voteCount++
		} else if err != nil {
			// fmt.Println("Error while solicting votes", err, address)
		}
	}
	return voteCount
}

func (cluster *Cluster) announceLeadership() {
	for _, peer := range cluster.Peers {
		peer.announceLeadership()
	}
}

func (cluster *Cluster) setLeader(address string) {
	peer := cluster.Peers[address]
	peer.RaftState = "leader"
}

func (cluster *Cluster) getUser(username string) (User, error) {
	leader := cluster.getLeader()
	return leader.getUser(username)
}

func (cluster *Cluster) voteOnLog(logEntry LogEntry) int {
	count := 0
	for _, peer := range cluster.Peers {
		vote, err := peer.voteOnLog(logEntry)
		if err == nil && vote {
			count++
		}
	}
	return count
}

func (cluster *Cluster) commitLog(logEntry LogEntry) {
	for _, peer := range cluster.Peers {
		peer.commitLog(logEntry)
	}
}

func (cluster *Cluster) proposeCommit(logEntry LogEntry) (bool, error) {
	leader := cluster.getLeader()
	return leader.proposeCommit(logEntry)
}

func (cluster *Cluster) getMaxID() (int, error) {
	leader := cluster.getLeader()
	return leader.getMaxID()
}

func (cluster *Cluster) getAllUsers() ([]User, error) {
	leader := cluster.getLeader()
	return leader.getAllUsers()
}

func (cluster *Cluster) getPostsBy(username string, timestamp int64) ([]Post, error) {
	leader := cluster.getLeader()
	return leader.getPostsBy(username, timestamp)
}
