package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

type State struct {
	ShardName        string
	Cluster          Cluster
	Shards           map[string]*Shard
	ExpectedShards   []string
	LocalAddress     string
	raftState        string
	heartbeatChecker *time.Timer
	voteTimer        *time.Timer
	ElectionTerm     int
	currentVote      string
	raftLog          RaftLog
}

func (state *State) initialize(shardName string, expectedShards []string, localAddress string) {
	state.ShardName = shardName
	state.ExpectedShards = expectedShards
	state.LocalAddress = localAddress
	if state.Shards == nil {
		state.Shards = make(map[string]*Shard)
	}
	state.raftLog = RaftLog{}
	state.raftLog.initialize()
	initStore()
	initSession()
}

func (state *State) update(receivedState State) error {
	if receivedState.ShardName == state.ShardName {
		state.Cluster.update(receivedState.LocalAddress)
		return nil
	}
	fmt.Println("New member is from a different shard")
	validShard := false
	for i := range state.ExpectedShards {
		if receivedState.ShardName == state.ExpectedShards[i] {
			validShard = true
			break
		}
	}
	if !validShard {
		fmt.Println("Invalid shard name received from new member - ", receivedState.ShardName, ". Expected shards are ", state.ExpectedShards)
		return errors.New("invalid shard name")
	}
	shard, exists := state.Shards[receivedState.ShardName]
	if !exists {
		shard = &Shard{}
		shard.initialize(receivedState.LocalAddress, receivedState.ShardName)
	} else {
		shard.update(receivedState.LocalAddress)
	}
	state.Shards[receivedState.ShardName] = shard
	return nil
}

func (state *State) updateFromMember(receivedState State) {
	fmt.Println("State from existing member ", receivedState)
	if receivedState.ShardName == state.ShardName {
		//Same shard processing goes here
		for address := range receivedState.Cluster.Peers {
			if address != receivedState.LocalAddress && address != state.LocalAddress {
				fmt.Println("Adding to cluster -", address)
				state.Cluster.update(address)
			}
		}
		state.Cluster.update(receivedState.LocalAddress)
		for shardName, shard := range receivedState.Shards {
			newShard := Shard{
				Name: shardName,
			}
			newShard.updateAll(shard.Cluster)
			state.Shards[shardName] = &newShard
		}
	} else {
		for shardName, shard := range receivedState.Shards {
			if shardName == state.ShardName {
				fmt.Println("Same shard data")
				for address := range shard.Cluster.Peers {
					if address != receivedState.LocalAddress && address != state.LocalAddress {
						state.Cluster.update(address)
					}
				}
				continue
			}
			newShard := Shard{
				Name: shardName,
			}
			newShard.updateAll(shard.Cluster)
			state.Shards[shardName] = &newShard
		}
		//Save the sender's shard too in the local state
		newShard := Shard{
			Name: receivedState.ShardName,
		}
		newShard.update(receivedState.LocalAddress)
		newShard.updateAll(receivedState.Cluster)
		state.Shards[receivedState.ShardName] = &newShard
	}

	//Announce to all
	dummyResponse := State{}
	for _, shard := range state.Shards {
		for address, peer := range shard.Cluster.Peers {
			if address != receivedState.LocalAddress && address != state.LocalAddress && peer.client != nil {
				err := peer.client.Call("ShardAccessor.Announce", state, &dummyResponse)
				if err != nil {
					fmt.Println("Error while announcing to - ", address, ": ", err)
				} else {
					fmt.Println("Announced to - ", address)
				}
			}
		}
	}
	for address, peer := range state.Cluster.Peers {
		if address != receivedState.LocalAddress && address != state.LocalAddress && peer.client != nil {
			err := peer.client.Call("ShardAccessor.Announce", state, &dummyResponse)
			if err != nil {
				fmt.Println("Error while announcing to - ", address, ": ", err)
			} else {
				fmt.Println("Announced to - ", address)
			}
		}
	}
}

func (state *State) updateClusterData() {
	if len(state.Cluster.Peers) == 0 {
		fmt.Println("This is the first peer and leader of the cluster-", state.ShardName)
		state.setLeader()
		state.ElectionTerm = 0
		return
	}
	state.Cluster.updateRaftState()
	leader := state.Cluster.getLeader()
	state.setFollower(false)
	fmt.Println("After updating raft state, leader is -\n", leader)
	state.ElectionTerm, _ = leader.getElectionTerm()
}

func (state *State) updateOtherShards() {
	for name, shard := range state.Shards {
		fmt.Println("State updated for other shard ", name)
		shard.Cluster.updateRaftState()
	}
}

func (state *State) setLeader() {
	if state.voteTimer != nil {
		state.voteTimer.Stop()
		fmt.Println("Stopped vote timer")
	}
	if state.heartbeatChecker != nil {
		state.heartbeatChecker.Stop()
		fmt.Println("Stopped heartbeat timer")
	}
	state.raftState = "leader"
	time.AfterFunc(time.Millisecond*100, state.sendHeartbeat)
	state.Cluster.announceLeadership()
}

func (state *State) setFollower(resetTimeout bool) {
	if resetTimeout && state.heartbeatChecker != nil {
		state.heartbeatChecker.Stop()
	}
	state.currentVote = ""
	state.raftState = "follower"
	timeout := time.Duration(rand.Intn(maxElectionTimeout-minElectionTimeout) + minElectionTimeout)
	state.heartbeatChecker = time.AfterFunc(time.Millisecond*timeout, state.checkHeartbeat)
}

func (state *State) sendHeartbeat() {
	state.Cluster.sendHeartbeat()
	time.AfterFunc(time.Millisecond*100, state.sendHeartbeat)
}

func (state *State) resetHeartbeatTimer() {
	if state.heartbeatChecker != nil {
		state.heartbeatChecker.Stop()
	}
	state.setFollower(false)
}

func (state *State) checkHeartbeat() {
	//Might already have voted for someone else
	if state.raftState == "follower" {
		state.setCandidate()
	}
}

func (state *State) setCandidate() {
	fmt.Println("HEARTBEAT NOT RECEIVED")
	//vote for itself before soliciting votes
	selfVote := state.vote(state.ElectionTerm+1, localAddress)
	if !selfVote {
		fmt.Println("Already voted for ", state.currentVote)
		return
	}
	state.raftState = "candidate"
	fmt.Println("Voting for itself ", localState.ElectionTerm)
	allVotes := state.Cluster.getVotes() + 1
	fmt.Println("All votes count is ", allVotes, len(state.Cluster.Peers)/2+1, state.Cluster.Count/2+1)
	if allVotes > state.Cluster.Count/2+1 {
		fmt.Println("I have won the election")
		state.setLeader()
	} else {
		fmt.Println("I have lost the election")
	}
}

func (state *State) vote(ElectionTerm int, address string) bool {
	fmt.Println(ElectionTerm, state.ElectionTerm, state.currentVote, state.raftState)
	if ElectionTerm > state.ElectionTerm && state.currentVote == "" && state.raftState == "follower" {
		if state.voteTimer != nil {
			state.voteTimer.Stop()
		}
		state.currentVote = address
		state.ElectionTerm = ElectionTerm
		//Use a larger timeout for voting
		timeout := time.Duration((rand.Intn(maxElectionTimeout-minElectionTimeout) + minElectionTimeout) * 2)
		state.voteTimer = time.AfterFunc(timeout*time.Millisecond, state.resetVoteTimer)
		return true
	}
	return false
}

func (state *State) resetVoteTimer() {
	state.currentVote = ""
	if state.raftState != "candidate" {
		//Voting has successfully resolved, no further action needed
		return
	}
	//Start another election after resetting previous vote
	fmt.Println("From reset vote timer")
	state.setCandidate()
}

func (state *State) getServerForUsername(username string) string {
	shardNumber := getUsernameHash(username)
	shardName := state.ExpectedShards[shardNumber]
	fmt.Println("shard is ", shardName, localState.ShardName)
	if shardName == localState.ShardName {
		return localAddress
	}
	shardAddress := state.Shards[shardName].Cluster.getLeader().Address
	if shardAddress == "" {
		state.Shards[shardName].Cluster.updateRaftState()
	}
	shardAddress = state.Shards[shardName].Cluster.getLeader().Address
	return shardAddress
}

func (state *State) getUser(username string) (User, error) {
	shardNumber := getUsernameHash(username)
	shardName := state.ExpectedShards[shardNumber]
	fmt.Println("Shard num and name are ", shardNumber, shardName)
	var user User
	var err error
	if shardName == state.ShardName {
		store := getStore()
		user, err = store.getUser(username)
	} else {
		shard := state.Shards[shardName]
		user, err = shard.Cluster.getUser(username)
	}
	return user, err
}

func (state *State) createUser(user User) {
	shardNumber := getUsernameHash(user.Username)
	shardName := state.ExpectedShards[shardNumber]
	if shardName == state.ShardName {
		userHandler := UserHandler{
			user: user,
		}
		userHandler.create()
	} else {
		//Create user in other shard
	}
}

func (state *State) subscribe(subscriber string, publisher string) error {
	fmt.Println(subscriber, " is subscribing to ", publisher)
	publisherShardNumber := getUsernameHash(publisher)
	publisherShardName := state.ExpectedShards[publisherShardNumber]
	var err error
	var publisherUser User
	if publisherShardName == state.ShardName {
		//Publisher is in the same shard
		publisherUser, err = state.getUser(publisher)
	} else {
		publisherUser, err = state.Cluster.getUser(publisher)
	}
	if err != nil {
		return err
	}
	subscriberUser, err := state.getUser(subscriber)
	if err != nil {
		return err
	}
	//publisher and subscriber exist, can subscribe now
	userHandler := UserHandler{
		user: subscriberUser,
	}
	userHandler.subscribe(publisherUser)
	return nil
}

func (state *State) post(username string, content string) error {
	fmt.Println("New post by ", username)
	user, err := state.getUser(username)
	if err != nil {
		return err
	}
	userHandler := UserHandler{user: user}
	userHandler.post(content)
	return nil
}

func (state *State) login(user User) error {
	registeredUser, err := state.getUser(user.Username)
	if err != nil {
		return err
	}
	userHandler := UserHandler{user: registeredUser}
	loginError := userHandler.verifyCredentials(user.Password)
	if loginError != nil {
		return loginError
	}
	session := getSession()
	session.addUser(registeredUser)
	return nil
}

func (state *State) getAllUsers() ([]User, error) {
	allUsers := []User{}
	store := getStore()
	localUsers, err := store.getAllUsers()
	if err != nil {
		return allUsers, err
	}
	allUsers = append(allUsers, localUsers...)
	for _, shard := range state.Shards {
		if shard.Cluster.Count == 0 {
			continue
		}
		shardUsers, err := shard.Cluster.getAllUsers()
		if err != nil {
			return allUsers, err
		}
		allUsers = append(allUsers, shardUsers...)
	}
	return allUsers, nil
}

func (state *State) getPublishers(username string) ([]string, error) {
	store := getStore()
	user, err := store.getUser(username)
	publishers := []string{}
	if err != nil {
		return publishers, err
	}
	publishers, err = store.getPublishers(user.ID)
	return publishers, err
}

func (state *State) getShard(shardNumber int) (bool, Shard) {
	shardName := state.ExpectedShards[shardNumber]
	if shardName == state.ShardName {
		return false, Shard{}
	}
	return true, *state.Shards[shardName]
}
