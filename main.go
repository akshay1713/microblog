package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"net"
	"net/rpc"
	"os"
	"strconv"
)

var localAddress string
var localState State
var shardAccessor ShardAccessor

func main() {
	rpc.Register(new(ShardAccessor))
	rpc.Register(new(PeerAccessor))
	rpc.Register(new(UserAccessor))
	fmt.Println("Starting")
	config, shard, memberAddress, recvPort, err := getCmdParams()
	if err != nil {
		fmt.Println("Error while initializing", err)
		return
	}
	saveLocalAddress(recvPort)
	localState.initialize(shard, config.Shards, localAddress)
	if memberAddress == "" {
		fmt.Println("First node in network")
		localState.updateClusterData()
	} else {
		go joinNetwork(memberAddress)
	}
	startServer(recvPort)
}

func joinNetwork(memberAddress string) {
	fmt.Println("Joining ", memberAddress)
	client, err := rpc.Dial("tcp", memberAddress)
	defer client.Close()
	if err != nil {
		panic(err)
	}
	var responseState State
	err = client.Call("ShardAccessor.Announce", &localState, &responseState)
	if err != nil {
		panic(err)
	}
	localState.updateFromMember(responseState)
	localState.updateClusterData()
	localState.updateOtherShards()
}

func saveLocalAddress(recvPort int) {
	localIP := getIP()
	localAddress = localIP + ":" + strconv.Itoa(recvPort)
}

func startServer(recvPort int) {
	fmt.Println("Starting server on ", localAddress)
	recvPortString := ":" + strconv.Itoa(recvPort)
	listen, err := net.Listen("tcp", recvPortString)
	if err != nil {
		fmt.Println("Error while starting server", err)
		return
	}
	for {
		conn, err := listen.Accept()
		if err != nil {
			fmt.Println("Error while accepting connection ", err)
			continue
		}
		handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	fmt.Println("Connection accepted from ", conn.RemoteAddr().String())
	go rpc.ServeConn(conn)
}

func getCmdParams() (Config, string, string, int, error) {
	configFileName := flag.String("c", "", "JSON config file")
	shardName := flag.String("shard", "", "Shard name")
	recvPort := flag.Int("recv", 4000, "Port to listen on")
	member := flag.String("address", "", "Address of existing node, if needed. This node will be the first one if this parameter is empty.")
	flag.Parse()
	config := Config{}
	if *configFileName == "" {
		return config, *shardName, *member, *recvPort, errors.New("A config file name is required")
	}
	configFile, err := os.Open(*configFileName)
	if err != nil {
		return config, *shardName, *member, *recvPort, err
	}
	decoder := json.NewDecoder(configFile)
	err = decoder.Decode(&config)
	if err != nil {
		return config, *shardName, *member, *recvPort, err
	}
	if len(config.Shards) == 0 {
		fmt.Println("")
		return config, *shardName, *member, *recvPort, errors.New("No shards specified in config")
	}
	if !isShardValid(*shardName, config.Shards) {
		fmt.Println(*shardName, " is not a valid shard. Please enter a name from the config. Valid shard names are ", config.Shards)
		return config, *shardName, *member, *recvPort, errors.New(fmt.Sprintf("%s is not a valid shard name. Valid shard names are given in the config file.", *shardName))
	}
	if *member == "" {
		fmt.Println("First node in network")
	}
	return config, *shardName, *member, *recvPort, err
}

func isShardValid(shardName string, shards []string) bool {
	if shardName == "" {
		return false
	}
	for i := range shards {
		if shards[i] == shardName {
			return true
		}
	}
	return false
}

func getIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}
