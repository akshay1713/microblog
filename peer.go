package main

import "net/rpc"

type Peer struct {
	Address   string
	client    *rpc.Client
	RaftState string
}

func (peer *Peer) getState(forceUpdate bool) (string, error) {
	if forceUpdate || peer.RaftState == "" {
		var latestState string
		err := peer.client.Call("PeerAccessor.GetState", &localState, &latestState)
		if err != nil {
			return "", err
		}
		peer.RaftState = latestState
	}
	return peer.RaftState, nil
}

func (peer *Peer) getElectionTerm() (int, error) {
	var ElectionTerm int
	err := peer.client.Call("PeerAccessor.GetElectionTerm", &localAddress, &ElectionTerm)
	return ElectionTerm, err
}

func (peer *Peer) isLeader() bool {
	return peer.RaftState == "leader"
}

func (peer *Peer) sendHeartbeat() bool {
	var ack bool
	err := peer.client.Call("PeerAccessor.Heartbeat", &localAddress, &ack)
	if err != nil && localState.ElectionTerm > 0 {
		// fmt.Println("Error while sending heartbeat ", err)
	}
	return ack
}

func (peer *Peer) getVote() (bool, error) {
	vote := false
	err := peer.client.Call("PeerAccessor.GetVote", &localState, &vote)
	return vote, err
}

func (peer *Peer) voteOnLog(logEntry LogEntry) (bool, error) {
	vote := false
	err := peer.client.Call("PeerAccessor.VoteOnLog", &logEntry, &vote)
	return vote, err
}

func (peer *Peer) commitLog(logEntry LogEntry) error {
	ack := false
	return peer.client.Call("PeerAccessor.CommitLog", &logEntry, &ack)
}

func (peer *Peer) announceLeadership() (bool, error) {
	ack := false
	err := peer.client.Call("PeerAccessor.AnnounceLeadership", &localState, &ack)
	return ack, err
}

func (peer *Peer) getUser(username string) (User, error) {
	var user User
	err := peer.client.Call("PeerAccessor.GetUser", &username, &user)
	return user, err
}

func (peer *Peer) proposeCommit(logEntry LogEntry) (bool, error) {
	ack := false
	err := peer.client.Call("PeerAccessor.ProposeCommit", &logEntry, &ack)
	return ack, err
}

func (peer *Peer) getMaxID() (int, error) {
	var max int
	dummyArg := false
	err := peer.client.Call("PeerAccessor.GetMaxID", &dummyArg, &max)
	return max, err
}

func (peer *Peer) getAllUsers() ([]User, error) {
	ack := false
	var users []User
	err := peer.client.Call("PeerAccessor.GetAllUsers", &users, &ack)
	return users, err
}

func (peer *Peer) getPostsBy(username string, searchTime int64) ([]Post, error) {
	var posts []Post
	searchParams := make([]interface{}, 2)
	searchParams[0] = &username
	searchParams[1] = &searchTime
	err := peer.client.Call("PeerAccessor.GetPostsBy", searchParams, &posts)
	return posts, err
}
