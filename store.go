package main

import (
	"database/sql"
	"fmt"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

type Store struct {
	database    *sql.DB
	userTable   string
	userIDTable string
	postTable   string
}

var store Store

func initStore() {
	store.init()
}

func getStore() Store {
	return store
}

func (store *Store) init() {
	store.openDatabase()
	database := store.database
	store.userTable = strings.Replace(strings.Replace(localAddress+"_"+userTable, ".", "_", -1), ":", "_", -1)
	store.postTable = strings.Replace(strings.Replace(localAddress+"_"+postTable, ".", "_", -1), ":", "_", -1)
	query := fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (id INTEGER PRIMARY KEY, username string, password string, subscribedTo text)", store.userTable)
	statement, err := database.Prepare(query)
	if err != nil {
		panic(err)
	}
	_, err = statement.Exec()
	if err != nil {
		panic(err)
	}
	query = fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (id INTEGER PRIMARY KEY, content TEXT, userid INTEGER, timestamp integer, FOREIGN KEY(userid) REFERENCES `%s`(ID))", store.postTable, store.userTable)
	statement, err = database.Prepare(query)
	if err != nil {
		panic(err)
	}
	_, err = statement.Exec()
	if err != nil {
		panic(err)
	}
	store.userIDTable = strings.Replace(strings.Replace(localAddress+"_"+userIDTable, ".", "_", -1), ":", "_", -1)
	query = fmt.Sprintf("DROP TABLE IF EXISTS `%s`", store.userIDTable)
	statement, err = database.Prepare(query)
	statement.Exec()

	query = fmt.Sprintf("CREATE TABLE `%s` (max_id INTEGER)", store.userIDTable)
	statement, err = database.Prepare(query)
	if err != nil {
		panic(err)
	}
	_, err = statement.Exec()
	if err != nil {
		panic(err)
	}
	store.setInitialMaxID()
}

func (store *Store) openDatabase() {
	if store.database != nil {
		store.database.Close()
	}
	database, err := sql.Open("sqlite3", userDbName)
	if err != nil {
		panic(err)
	}
	store.database = database
}

func (store *Store) setInitialMaxID() {
	query := fmt.Sprintf("SELECT MAX(id) as max FROM `%s`", store.userTable)
	statement, err := store.database.Prepare(query)
	if err != nil {
		panic(err)
	}
	defer statement.Close()
	var max int
	statement.QueryRow().Scan(&max)
	query = fmt.Sprintf("INSERT INTO `%s` values(%d) ", store.userIDTable, max)
	statement, err = store.database.Prepare(query)
	_, err = statement.Exec()
	if err != nil {
		panic(err)
	}
}

func (store *Store) setMaxID(maxID int) {
	query := fmt.Sprintf("UPDATE  `%s` set max_id = %d", store.userIDTable, maxID)
	statement, _ := store.database.Prepare(query)
	statement.Exec()
	statement.Close()
}

func (store *Store) incrementMaxID() error {
	query := fmt.Sprintf("SELECT max_id FROM `%s` LIMIT 1", store.userIDTable)
	statement, err := store.database.Prepare(query)
	if err != nil {
		return err
	}
	defer statement.Close()
	var max int
	statement.QueryRow().Scan(&max)
	query = fmt.Sprintf("UPDATE `%s` set max_id = %d", store.userIDTable, max+1)
	statement, err = store.database.Prepare(query)
	if err != nil {
		fmt.Println("error ", err)
		return err
	}
	_, err = statement.Exec()
	return err
}

func (store *Store) getMaxUserID() (int, error) {
	query := fmt.Sprintf("SELECT max_id from `%s` LIMIT 1", store.userIDTable)
	statement, err := store.database.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer statement.Close()
	var maxID int
	err = statement.QueryRow().Scan(&maxID)
	if err != nil {
		return maxID, err
	}
	err = store.incrementMaxID()
	return maxID, err
}

func (store *Store) getUser(username string) (User, error) {
	query := fmt.Sprintf("SELECT username, password, id FROM `%s` WHERE username = '%s' limit 1", store.userTable, username)
	statement, err := store.database.Prepare(query)
	if err != nil {
		return User{}, err
	}
	defer statement.Close()
	result, err := statement.Query()
	defer result.Close()
	if err != nil {
		return User{}, err
	}
	result.Next()
	user := User{}
	result.Scan(&user.Username, &user.Password, &user.ID)
	return user, nil
}

func (store *Store) createUser(username string, password string, userID int) (int, error) {
	query := fmt.Sprintf("INSERT INTO `%s` values (%d, '%s', '%s', '')", store.userTable, userID, username, password)
	statement, err := store.database.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer statement.Close()
	result, err := statement.Exec()
	if err != nil {
		return 0, err
	}
	id, _ := result.LastInsertId()
	store.setMaxID(userID)
	return int(id), err
}

func (store *Store) createPost(content string, userID int, timestamp int64) (int, error) {
	query := fmt.Sprintf("INSERT INTO `%s` values (null, '%s', %d, %d)", store.postTable, content, userID, timestamp)
	statement, err := store.database.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer statement.Close()
	result, err := statement.Exec()
	if err != nil {
		return 0, err
	}
	id, _ := result.LastInsertId()
	return int(id), err
}

func (store *Store) getPosts(userID int, timestamp int64) ([]Post, error) {
	query := fmt.Sprintf("SELECT id, content, timestamp from `%s` where userid = %d and timestamp > %d", store.postTable, userID, timestamp)
	statement, err := store.database.Prepare(query)
	if err != nil {
		return []Post{}, err
	}
	defer statement.Close()
	results, err := statement.Query()
	if err != nil {
		return []Post{}, err
	}
	defer results.Close()
	posts := []Post{}
	var content string
	var id int
	var postTime int64
	for results.Next() {
		err = results.Scan(&id, &content, &postTime)
		if err != nil {
			return posts, err
		}
		posts = append(posts, Post{Content: content, AuthorID: userID, Timestamp: postTime, ID: id})
	}
	return posts, nil
}

func (store *Store) subscribeUser(subscriberID int, publisherUsername string) error {
	currentUsernames, err := store.getPublishers(subscriberID)
	if err != nil {
		return err
	}
	currentUsernames = append(currentUsernames, publisherUsername)
	subscribedTo := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(currentUsernames)), ","), "[]")
	query := fmt.Sprintf("UPDATE `%s` set subscribedTo = '%s' where id = %d", store.userTable, subscribedTo, subscriberID)
	statement, err := store.database.Prepare(query)
	if err != nil {
		return err
	}
	_, err = statement.Exec()
	return err
}

func (store *Store) getAllUsers() ([]User, error) {
	query := fmt.Sprintf("SELECT id, username FROM `%s`", store.userTable)
	statement, err := store.database.Prepare(query)
	users := []User{}
	if err != nil {
		return users, err
	}
	defer statement.Close()
	results, err := statement.Query()
	if err != nil {
		return users, err
	}
	defer results.Close()
	for results.Next() {
		user := User{}
		results.Scan(&user.ID, &user.Username)
		users = append(users, user)
	}
	return users, nil
}

func (store *Store) getPublishers(subscriberID int) ([]string, error) {
	fmt.Println("Getting publishers for", subscriberID)
	query := fmt.Sprintf("SELECT subscribedTo from `%s` where id = %d", store.userTable, subscriberID)
	statement, err := store.database.Prepare(query)
	if err != nil {
		return []string{}, err
	}
	defer statement.Close()
	var currentUsernames []string
	var usernamesJoined string
	err = statement.QueryRow().Scan(&usernamesJoined)
	if err != nil {
		return []string{}, err
	}
	if len(usernamesJoined) > 0 {
		usernames := strings.Split(usernamesJoined, ",")
		for i := range usernames {
			currentUsernames = append(currentUsernames, usernames[i])
		}
	}
	return currentUsernames, nil
}
