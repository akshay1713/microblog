package main

import "fmt"
import "bufio"
import "os"
import "strings"
import "flag"
import "net/rpc"
import "time"
import "os/signal"
import "syscall"

var serverClient *rpc.Client
var serverAddress string

var maxDisplayedTime int64

type User struct {
	Username string
	Password string
	ID       int
}

type Post struct {
	AuthorID   int
	Timestamp  int64
	Content    string
	ID         int
	AuthorName string
}

type Feed struct {
	state            string
	maxIDs           map[string]int
	interrupt        chan os.Signal
	maxDisplayedTime int64
}

func (feed *Feed) feedInterrupt() {
	<-feed.interrupt
	fmt.Println("Stopping feed")
	feed.stopPolling()
}

func (feed *Feed) startPolling() {
	feed.state = "polling"
	feed.maxDisplayedTime = 0
	feed.interrupt = make(chan os.Signal, 2)
	signal.Notify(feed.interrupt, os.Interrupt, syscall.SIGTERM)
	go feed.feedInterrupt()
	feed.printNextPosts()
}

func (feed *Feed) stopPolling() {
	feed.state = "idle"
}

func (feed *Feed) isPolling() bool {
	return feed.state == "polling"
}

func (feed *Feed) printNextPosts() {
	if !feed.isPolling() {
		return
	}
	posts := []Post{}
	err := serverClient.Call("UserAccessor.GetPosts", &clientState.loggedInUser.Username, &posts)
	newMaxTime := feed.maxDisplayedTime
	if err != nil {
		fmt.Println("Error while getting posts:", err)
	} else {
		for i := range posts {
			if posts[i].Timestamp > feed.maxDisplayedTime {
				fmt.Println(posts[i].Content)
				fmt.Println("-", posts[i].AuthorName)
				if posts[i].Timestamp > newMaxTime {
					newMaxTime = posts[i].Timestamp
				}
			}
		}
	}
	feed.maxDisplayedTime = newMaxTime
	time.AfterFunc(time.Second*5, feed.printNextPosts)
}

type ClientState struct {
	state        string
	loggedInUser User
}

func (clientState *ClientState) login(username string) {
	clientState.state = "loggedin"
	fmt.Println("Successfully loggedin")
	loggedInUser := User{Username: username}
	clientState.loggedInUser = loggedInUser
}

func (clientState *ClientState) isLoggedIn() bool {
	fmt.Println(clientState.state)
	return clientState.state == "loggedin"
}

var clientState ClientState
var feed Feed

func initState() {
	clientState = ClientState{
		state: "not_loggedin",
	}
	feed = Feed{
		state: "idle",
	}
}

func main() {
	fmt.Println("Starting client")
	initState()
	address := flag.String("address", "", "Server address")
	flag.Parse()
	fmt.Println("Connecting to ", *address)
	err := connectToServer(*address)
	if err != nil {
		fmt.Println("Error while connecting to the server: ", err)
		return
	}
	startCli()
}

func connectToServer(address string) error {
	var err error
	serverClient, err = rpc.Dial("tcp", address)
	return err
}

func startCli() {
	var input string
	reader := bufio.NewReader(os.Stdin)
	for true {
		fmt.Println("Waiting for the next command. ")
		input, _ = reader.ReadString('\n')
		input = strings.TrimSpace(input)
		handleCommand(input)
	}
}

func handleCommand(input string) {
	switch input {
	case "register":
		fmt.Println("Registering")
		username := getUsername()
		password := getPassword()
		register(username, password)
	case "login":
		fmt.Println("Logging in")
		login(getUsername(), getPassword())
	case "feed":
		startFeed()
	case "list":
		listUsers()
	case "publishers":
		listPublishers()
	case "subscribe":
		subscribe(getUsername())
	case "post":
		post()
	case "exit":
		os.Exit(0)
	}
}

func getUsername() string {
	return getInput("Enter username: ")
}

func getPassword() string {
	return getInput("Enter password: ")
}

func getInput(prompt string) string {
	var input string
	fmt.Println(prompt)
	reader := bufio.NewReader(os.Stdin)
	input, _ = reader.ReadString('\n')
	input = strings.TrimSpace(input)
	return input
}

func register(username string, password string) {
	err := serverCheck(username)
	if err != nil {
		fmt.Println("Error while connecting to server", err)
		return
	}
	var args User
	args.Username = username
	args.Password = password
	var created bool
	err = serverClient.Call("UserAccessor.Register", &args, &created)
	if err != nil {
		fmt.Println("Error while connecting to server ", err)
	}
	if created && err == nil {
		fmt.Println("Created user")
		return
	}
	if err != nil {
		fmt.Println("Error while creating user: ", err)
	}
}

func login(username string, password string) {
	err := serverCheck(username)
	if err != nil {
		fmt.Println("Error while connecting to server ", err)
		return
	}
	var args User
	args.Username = username
	args.Password = password
	loggedIn := false
	err = serverClient.Call("UserAccessor.Login", &args, &loggedIn)
	if err != nil || !loggedIn {
		fmt.Println("Unable to login ", err)
		return
	}
	if loggedIn {
		clientState.login(username)
	}
}

func subscribe(username string) {
	fmt.Println("Subscribing to", username)
	ack := false
	subscriptionArgs := []string{clientState.loggedInUser.Username, username}
	err := serverClient.Call("UserAccessor.Subscribe", &subscriptionArgs, &ack)
	if err != nil || !ack {
		fmt.Println("Error while subscribing:", err)
	} else {
		fmt.Println("Subscription successful")
	}
}

func post() {
	postContent := getInput("Enter content:")
	postArgs := []string{clientState.loggedInUser.Username, postContent}
	ack := false
	err := serverClient.Call("UserAccessor.Post", &postArgs, &ack)
	if err != nil || !ack {
		fmt.Println("Error while posting:", err)
	} else {
		fmt.Println("Successfully posted")
	}
}

func startFeed() {
	if !clientState.isLoggedIn() {
		fmt.Println("You are not logged in")
		return
	}
	if feed.isPolling() {
		return
	}
	feed.startPolling()
}

func listUsers() {
	if !clientState.isLoggedIn() {
		fmt.Println("You are not logged in")
		return
	}
	allUsers := []User{}
	dummyArg := false
	err := serverClient.Call("UserAccessor.GetAllUsers", &dummyArg, &allUsers)
	if err != nil {
		fmt.Println("Unable to fetch users ", err)
		return
	}
	fmt.Println("Users -")
	for i := range allUsers {
		if allUsers[i].Username == clientState.loggedInUser.Username {
			continue
		}
		fmt.Println(allUsers[i].Username)
	}
}

func listPublishers() {
	if !clientState.isLoggedIn() {
		fmt.Println("You are not logged in")
		return
	}
	publishers := []string{}
	username := clientState.loggedInUser.Username
	err := serverClient.Call("UserAccessor.GetPublishers", &username, &publishers)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}
	fmt.Println("Currently subscribed to:")
	for i := range publishers {
		fmt.Println(publishers[i])
	}
}

func serverCheck(username string) error {
	var serverForUsername string
	err := serverClient.Call("UserAccessor.GetServer", &username, &serverForUsername)
	if err != nil {
		return err
	}
	serverAddress = strings.TrimRight(strings.TrimSpace(serverAddress), "\r\n")
	serverForUsername = strings.TrimRight(strings.TrimSpace(serverForUsername), "\r\n")
	if serverForUsername != serverAddress {
		serverAddress = serverForUsername
		err = connectToServer(serverAddress)
		if err != nil {
			return err
		}
		fmt.Println("Connected to another server", serverAddress)
	}
	return err
}
