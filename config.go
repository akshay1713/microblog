package main

type Config struct {
	Shards []string
	Cost   int
	Salt   string
}
