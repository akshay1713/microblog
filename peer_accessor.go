package main

import "fmt"

type PeerAccessor struct{}

func (peerAccessor *PeerAccessor) GetState(receivedState *State, raftState *string) error {
	*raftState = localState.raftState
	fmt.Println("Returning state ", *raftState)
	return nil
}

func (peerAccessor *PeerAccessor) Heartbeat(peerAddress *string, ack *bool) error {
	localState.resetHeartbeatTimer()
	*ack = true
	return nil
}

func (peerAccessor *PeerAccessor) GetElectionTerm(peerAddress *string, ElectionTerm *int) error {
	*ElectionTerm = localState.ElectionTerm
	return nil
}

func (peerAccessor *PeerAccessor) GetVote(peerState *State, vote *bool) error {
	*vote = localState.vote(peerState.ElectionTerm, peerState.LocalAddress)
	fmt.Println("For: ", peerState.LocalAddress, *vote, localState.currentVote, localState.raftState)
	return nil
}

func (peerAccessor *PeerAccessor) AnnounceLeadership(peerState *State, ack *bool) error {
	fmt.Println(peerState.LocalAddress, " is the new leader")
	localState.Cluster.setLeader(peerState.LocalAddress)
	localState.setFollower(true)
	return nil
}

func (peerAccessor *PeerAccessor) GetUser(username *string, user *User) error {
	fmt.Println("Searching for ", *username)
	store := getStore()
	foundUser, err := store.getUser(*username)
	*user = foundUser
	return err
}

func (peerAccessor *PeerAccessor) VoteOnLog(logEntry *LogEntry, vote *bool) error {
	localState.raftLog.add(*logEntry)
	*vote = true
	return nil
}

func (peerAccessor *PeerAccessor) CommitLog(logEntry *LogEntry, ack *bool) error {
	err := localState.raftLog.commit(logEntry.UUID)
	*ack = true
	return err
}

func (peerAccessor *PeerAccessor) ProposeCommit(logEntry *LogEntry, ack *bool) error {
	*ack = true
	localState.raftLog.addAndInitiateCommit(*logEntry)
	return nil
}

func (peerAccessor *PeerAccessor) GetMaxID(dummyArg *bool, max *int) error {
	store := getStore()
	var err error
	*max, err = store.getMaxUserID()
	return err
}

func (peerAccessor *PeerAccessor) GetAllUsers(dummyArg *bool, users *[]User) error {
	store := getStore()
	usersOnShard, err := store.getAllUsers()
	*users = usersOnShard
	return err
}

func (peerAccessor *PeerAccessor) GetPostsBy(searchArgs []interface{}, posts *[]Post) error {
	username := searchArgs[0].(*string)
	searchTime := searchArgs[1].(*int64)
	postManager := PostManager{}
	foundPosts, err := postManager.getPostsBy(*username, *searchTime)
	*posts = foundPosts
	return err
}
