package main

var (
	minElectionTimeout = 150
	maxElectionTimeout = 300
	logDbName          = "raftLog.db"
	userDbName         = "mblog.db"
	userTable          = "Users"
	userIDTable        = "UserID"
	postTable          = "Posts"
)
