package main

import (
	"errors"
	"fmt"
	"hash/fnv"

	"golang.org/x/crypto/bcrypt"
)

type PasswordHandler struct {
	cost int
	//randomly generate unique salts every time, but it's a pain so do it later if this goes anywhere
	salt string
}

var savedPasswordHandler PasswordHandler

func initPasswordHandler(cost int, salt string) {
	savedPasswordHandler = PasswordHandler{cost: cost, salt: salt}
}

func (passwordHandler PasswordHandler) getHashedPassword(password string) string {
	saltedPwd := []byte(password + passwordHandler.salt)
	hashedPassword, _ := bcrypt.GenerateFromPassword(saltedPwd, passwordHandler.cost)
	return string(hashedPassword)
}

func (passwordHandler PasswordHandler) compare(actualPassword string, givenPassword string) error {
	saltedGivenPassword := givenPassword + passwordHandler.salt
	return bcrypt.CompareHashAndPassword([]byte(actualPassword), []byte(saltedGivenPassword))
}

type UserHandler struct {
	user User
}

func (userHandler *UserHandler) verifyCredentials(givenPassword string) error {
	if savedPasswordHandler.compare(userHandler.user.Password, givenPassword) == nil {
		return nil
	}
	return errors.New("Invalid credentials")
}

func (userHandler *UserHandler) create() {
	fmt.Println("Creating user ", userHandler.user.Username)
	store := getStore()
	userHandler.user.Password = savedPasswordHandler.getHashedPassword(userHandler.user.Password)
	maxID, _ := store.getMaxUserID()
	for _, shard := range localState.Shards {
		shardMaxID, _ := shard.Cluster.getMaxID()
		if shardMaxID > maxID {
			maxID = shardMaxID
		}
	}
	userHandler.user.ID = maxID + 1
	logEntry := localState.raftLog.genEntryForUserCreation(userHandler.user)
	localState.raftLog.addAndInitiateCommit(logEntry)
}

func (userHandler *UserHandler) subscribe(publisher User) {
	logEntry := localState.raftLog.genEntryForSubscription(userHandler.user, publisher)
	localState.raftLog.addAndInitiateCommit(logEntry)
}

func (userHandler *UserHandler) post(content string) {
	logEntry := localState.raftLog.genEntryForPost(userHandler.user, content)
	localState.raftLog.addAndInitiateCommit(logEntry)
}

func (userHandler *UserHandler) getPublishers() ([]string, error) {
	store := getStore()
	return store.getPublishers(userHandler.user.ID)
}

func getUsernameHash(username string) int {
	hasher := fnv.New32()
	hasher.Write([]byte(username))
	return int(hasher.Sum32() % uint32(len(localState.ExpectedShards)))
}

func getShardForUsername(username string) (bool, Shard) {
	shardNumber := getUsernameHash(username)
	return localState.getShard(shardNumber)
}

type User struct {
	Username string
	Password string
	ID       int
}
