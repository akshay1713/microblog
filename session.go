package main

import "fmt"
import "time"

type UserSession struct {
	user       User
	lastCall   int64
	publishers []string
}

type Session struct {
	loggedInUsers   map[string]UserSession
	latestPosts     map[string][]Post
	previousPosts   map[string][]Post
	lastRefreshTime int64
}

func (session *Session) addUser(user User) {
	user.Password = ""
	userSession := UserSession{
		user:     user,
		lastCall: time.Now().Unix(),
	}
	store := getStore()
	publishers, err := store.getPublishers(user.ID)
	if err != nil {
		fmt.Println("Error while adding user to session:", err)
	}
	// fmt.Println("Publishers found", publishers)
	userSession.publishers = publishers
	session.loggedInUsers[user.Username] = userSession
	for i := range publishers {
		if _, exists := session.latestPosts[publishers[i]]; !exists {
			// fmt.Println("Adding publisher", publishers[i])
			session.latestPosts[publishers[i]] = []Post{}
			session.previousPosts[publishers[i]] = []Post{}
		}
	}
	fmt.Println(user.Username, " has loggedin")
}

func (session *Session) getPostsForUser(username string) []Post {
	userSession := session.loggedInUsers[username]
	publishers := userSession.publishers
	allPosts := []Post{}
	for i := range publishers {
		currentPosts := session.latestPosts[publishers[i]]
		previousPosts := session.previousPosts[publishers[i]]
		fmt.Println(publishers[i], currentPosts, previousPosts)
		allPosts = append(allPosts, session.latestPosts[publishers[i]]...)
		allPosts = append(allPosts, session.previousPosts[publishers[i]]...)
	}
	return allPosts
}

func (session *Session) updateUser(username string) {
	store := getStore()
	user, _ := store.getUser(username)
	session.addUser(user)
}

func (session *Session) refreshPosts() {
	postManager := PostManager{}
	for username, posts := range session.latestPosts {
		previousPosts, exists := session.previousPosts[username]
		if !exists {
			previousPosts = []Post{}
		}
		previousPosts = append(previousPosts, posts...)
		latestPosts, err := postManager.getPostsBy(username, session.lastRefreshTime)
		if err != nil {
			fmt.Println("Error while refreshing posts:", err)
		}
		session.previousPosts[username] = previousPosts
		session.latestPosts[username] = latestPosts
	}
	session.lastRefreshTime = time.Now().Unix()
	time.AfterFunc(time.Second*10, session.refreshPosts)
}

func (session *Session) clearPosts() {
	for username := range session.previousPosts {
		session.previousPosts[username] = []Post{}
	}
	time.AfterFunc(time.Second*30, session.clearPosts)
}

var session Session

func initSession() {
	session = Session{
		loggedInUsers:   make(map[string]UserSession),
		latestPosts:     make(map[string][]Post),
		previousPosts:   make(map[string][]Post),
		lastRefreshTime: time.Now().Unix(),
	}
	time.AfterFunc(time.Second*10, session.refreshPosts)
	time.AfterFunc(time.Second*30, session.clearPosts)
}

func getSession() Session {
	return session
}
