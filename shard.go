package main

type Shard struct {
	Cluster Cluster
	Name    string
}

//Initializes a new shard, with the given address as the first member of the shard cluster
func (shard *Shard) initialize(address string, name string) {
	shard.Name = name
	shard.Cluster = Cluster{}
	shard.Cluster.update(address)
}

func (shard *Shard) update(address string) {
	shard.Cluster.update(address)
}

func (shard *Shard) updateAll(cluster Cluster) {
	for address, _ := range cluster.Peers {
		shard.Cluster.update(address)
	}
}
